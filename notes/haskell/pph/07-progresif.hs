-- penghasilan kena pajak
pkps :: [Int]
pkps = [0, 50*10^6, 250*10^6, 500*10^6, 10^9]

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round $ pph_recursive $ fromIntegral pkp
  where
  -- [(tingkat, persentase)]
  reff    = [(0, 5), (50, 15), (250, 25), (500, 30)]
  tarrifs = [ (t*(10^6), (p/100)) | (t, p) <- reff]
  pph_recursive 0        = 0
  pph_recursive pkp_real = 
    (\(t, p) -> (pkp_real - t) * p + pph_recursive(t)) tariff
    where
    indices = [ i | (i, (t, p)) <- zip [0..] tarrifs, pkp_real > t]
    tariff  = tarrifs !! last indices    

main = mapM_ (print . pph) pkps
