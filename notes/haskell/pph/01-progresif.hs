-- penghasilan kena pajak
pkp :: Int
pkp = 10^9

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round $ pph_rec $ fromIntegral pkp
  where
    [t1, t2, t3]  = map (* 10^6) [50, 250, 500]
    pph_rec pkp_r = pkp_guard pkp_r
    pkp_guard pkp
      | pkp <= t1 = pkp * 0.05
      | pkp <= t2 = (pkp - t1) * 0.15 + pph_rec t1
      | pkp <= t3 = (pkp - t2) * 0.25 + pph_rec t2
      | otherwise = (pkp - t3) * 0.30 + pph_rec t3

main = do
   print $ pph pkp
