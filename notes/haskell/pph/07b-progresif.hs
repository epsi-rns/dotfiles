import Data.Foldable

-- penghasilan kena pajak
pkps :: [Int]
pkps = [0, 50*10^6, 250*10^6, 500*10^6, 10^9]

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round $ pph_recursive $ fromIntegral pkp
  where
    [t1, t2, t3, t4]  = map (* 10^6) [0, 50, 250, 500]
    [p1, p2, p3, p4]  = [0.05, 0.15, 0.25, 0.30]
    pph_recursive pkp_real 
      | pkp_real > t4  = (pkp_real - t4) * p4 + pph_recursive t4
      | pkp_real > t3  = (pkp_real - t3) * p3 + pph_recursive t3
      | pkp_real > t2  = (pkp_real - t2) * p2 + pph_recursive t2
      | pkp_real > t1  = (pkp_real - t1) * p1 + pph_recursive t1
      | otherwise      = 0

main = forM_ pkps (print . pph)
