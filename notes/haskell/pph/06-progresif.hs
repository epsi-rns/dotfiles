-- pemula haskell
import Data.Foldable

-- penghasilan kena pajak
pkps :: [Int]
pkps = [50*10^6, 250*10^6, 500*10^6, 10^9]

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round $
    (\(t, p, d) -> fromIntegral pkp * p - d * (10^6))
    (reff !! last indices)
  where
    -- [(tingkat, persentase, pengurang)]
    reff    = [(0, 0.05, 0), (50, 0.15, 5), (250, 0.25, 30), (500, 0.30, 55)]
    indices = [ i | (i, (t, p, d)) <- zip [0..] reff, pkp >= (t * 10^6) ]

main = forM_ pkps (print . pph)
