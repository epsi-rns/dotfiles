import Text.Printf

-- penghasilan kena pajak
pkps :: [Int]
pkps = [0, 50, 250, 500, 10000]

-- kali sejuta
million :: [Int] -> [Int]
million nums = [ i * 10^6 | i <- nums ]

-- cetak
main = mapM_ (printf "%18d\n") (million pkps)

