-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = maximum $ [
          round(fromIntegral(pkp) * (pph_p !! x) - (pph_dj !! x)) 
        | x <- [0, 1, 2, 3]]
    where
      pph_p  = [0.05, 0.15, 0.25, 0.30]                          -- persentase
      pph_dj = [fromIntegral(x * 1000000) | x <- [0, 5, 30, 55]] -- pengurang

main = do
   print $ pph pkp
