-- pemula haskell

pkp :: Int
pkp = 50

ppi :: Int -> Int
ppi pkpr = pkpr (pkp)
  where 
    pkpr pkp
      | pkp <=  50 = 1
      | pkp <= 250 = 2
      | pkp <= 500 = 3
      | otherwise  = 4

main = do
   print $ ppi(pkp)
