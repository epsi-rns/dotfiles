-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = maximum $ [round(x) | x <- pph_list]
    where
      -- Typecast, misalnya ke Double
      pkp_d  = fromIntegral(pkp)
      -- untuk menghindari error menulis digit
      juta   = 1000000
      -- persentase tarif
      pph_p  = [0.05, 0.15, 0.25, 0.30]
      -- pengurang, versi singkat
      pph_d  = [0, 5, 30, 55]
      -- pengurang, versi dalam juta rupiah
      pph_dj = [fromIntegral(x * juta) | x <- pph_d]
      -- daftar tingkat, index dari nol
      tingkat = [0, 1, 2, 3]
      -- perhitungan utama, semua tingkat dihitung
      pph_list = [pkp_d * (pph_p !! x) - (pph_dj !! x) | x <- tingkat]

main = do
   print $ pph pkp
