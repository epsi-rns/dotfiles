-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph_list :: [Int]
pph_list = [round(x) | x <- [t1 pkp, t2 pkp, t3 pkp, t4 pkp]]
    where
      t1 pkp = pkp_d * (pph_p !! 0) - (pph_dj !! 0)
      t2 pkp = pkp_d * (pph_p !! 1) - (pph_dj !! 1)
      t3 pkp = pkp_d * (pph_p !! 2) - (pph_dj !! 2)
      t4 pkp = pkp_d * (pph_p !! 3) - (pph_dj !! 3)
      -- Typecast, misalnya ke Double
      pkp_d  = fromIntegral(pkp)
      -- untuk menghindari error menulis digit
      juta   = 1000000
      -- persentase tarif
      pph_p  = [0.05, 0.15, 0.25, 0.30]
      -- pengurang, versi singkat
      pph_d  = [0, 5, 30, 55]
      -- pengurang, versi dalam juta rupiah
      pph_dj = [fromIntegral(x * juta) | x <- pph_d]

main = do
   print $ maximum $ pph_list
