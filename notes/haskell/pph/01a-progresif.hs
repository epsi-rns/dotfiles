-- penghasilan kena pajak
pkp :: Double
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
(t1, t2, t3) = (50000000, 250000000, 500000000)

pph :: Double -> Double
pph pkp = if pkp <= t1
             then pph_t1 pkp
             else pph_t2 pkp

pph_t1 :: Double -> Double
pph_t1 pkp = pkp * 0.05 

pph_t2 :: Double -> Double
pph_t2 pkp = if pkp <= t2 
                then (pkp - t1) * 0.15 + pph_t1(t1)
                else pph_t3 pkp

pph_t3 :: Double -> Double
pph_t3 pkp = if pkp <= t3
                then (pkp - t2) * 0.25 + pph_t2(t2)
                else pph_t4 pkp

pph_t4 :: Double -> Double
pph_t4 pkp = (pkp - t3) * 0.30 + pph_t3(t3)

main = do
   print $ round $ pph pkp

