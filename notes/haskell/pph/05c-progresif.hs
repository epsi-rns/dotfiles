-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = maximum $ 
  [ round (fromIntegral(pkp) * fst(x) - fromIntegral(snd(x)*1000000))
    | x <- [(0.05, 0), (0.15, 5), (0.25, 30), (0.30, 55)]
  ]

main = do
   print $ pph pkp
