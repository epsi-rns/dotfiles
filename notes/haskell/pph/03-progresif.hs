-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round(pkpr pkp)
  where 
    pkpr pkp
      | pkp > t4 = pkp_d * p4 - d4
      | pkp > t3 = pkp_d * p3 - d3
      | pkp > t2 = pkp_d * p2 - d2
      | pkp > t1 = pkp_d * p1 - d1
    -- ambang batas 
    [t1, t2, t3, t4] = map (*juta) [0, 50, 250, 500]
    -- persentase tarif
    (p1, p2, p3, p4) = (0.05, 0.15, 0.25, 0.30)
    -- pengurang, versi dalam juta rupiah
    [d1, d2, d3, d4] = map (fromIntegral . (*juta)) [0, 5, 30, 55]
    -- Typecast, misalnya ke Double
    pkp_d  = fromIntegral(pkp)
    -- untuk menghindari error menulis digit
    juta   = 1000000

main = putStrLn $ show $ pph pkp
