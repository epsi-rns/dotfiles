-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round(pkpr pkp)
  where 
    pkpr pkp
      | pkp > t4*juta = pkp_d * p4 - dj4
      | pkp > t3*juta = pkp_d * p3 - dj3
      | pkp > t2*juta = pkp_d * p2 - dj2
      | pkp > t1*juta = pkp_d * p1 - dj1
    -- ambang batas 
    (t1, t2, t3, t4) = (0, 50, 250, 500)
    -- persentase tarif
    (p1, p2, p3, p4) = (0.05, 0.15, 0.25, 0.30)
    -- pengurang, versi singkat
    (d1, d2, d3, d4) = (0, 5, 30, 55)
    -- pengurang, versi dalam juta rupiah
    [dj1, dj2, dj3, dj4] = 
      [fromIntegral(x * juta) | x <- [d1, d2, d3, d4]]
    -- Typecast, misalnya ke Double
    pkp_d  = fromIntegral(pkp)
    -- untuk menghindari error menulis digit
    juta   = 1000000

main = do
   print $ pph pkp
