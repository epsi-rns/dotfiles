-- taxable incomes
pkps :: [Int]
pkps = [10^7, 10^8, 10^9]

-- income tax using progressive tariff
pph :: Int -> Int
pph pkp = maximum $ [round(x) | x <- pph_list]
    where
      -- Typecast, such as to Double
      pkp_d = fromIntegral(pkp)
      -- to avoid typo in writing digit
      million = 10^6
      -- tariff rate in percent
      pph_p = [0.05, 0.15, 0.25, 0.30]
      -- difference, short version
      pph_d = [0, 5, 30, 55]
      -- difference, in million rupiah
      pph_dj = map (fromIntegral . (*million)) pph_d
      -- range list, zero based index
      level   = [0, 1, 2, 3]
      -- main calculation, calulate all level
      pph_list = [pkp_d * (pph_p !! x) - (pph_dj !! x) | x <- level]

main = print $ map pph pkps
