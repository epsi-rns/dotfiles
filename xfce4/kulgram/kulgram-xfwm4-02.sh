#!/bin/sh

# ANSI Color -- use these variables to easily have different color
#    and format output. Make sure to output the reset sequence after 
#    colors (f = foreground, b = background), and use the 'off'
#    feature for anything you turn on.

initializeANSI()
{
 esc=""

  blackf="${esc}[30m";   redf="${esc}[31m";    greenf="${esc}[32m"
  yellowf="${esc}[33m"   bluef="${esc}[34m";   purplef="${esc}[35m"
  cyanf="${esc}[36m";    whitef="${esc}[37m"
  
  blackb="${esc}[40m";   redb="${esc}[41m";    greenb="${esc}[42m"
  yellowb="${esc}[43m"   blueb="${esc}[44m";   purpleb="${esc}[45m"
  cyanb="${esc}[46m";    whiteb="${esc}[47m"

  boldon="${esc}[1m";    boldoff="${esc}[22m"
  italicson="${esc}[3m"; italicsoff="${esc}[23m"
  ulon="${esc}[4m";      uloff="${esc}[24m"
  invon="${esc}[7m";     invoff="${esc}[27m"

  reset="${esc}[0m"
}

hrz='──────────'
spc='          '

# note in this first use that switching colors doesn't require a reset
# first - the new color overrides the old one.

clear 

initializeANSI

cat << EOF
┌────${hrz}${hrz}${hrz}${hrz}${hrz}┐
│ ${boldon}${whitef}    ▄▄▄${reset}          │  Kulgram lanjutan: XFWM4 Theme    │
│ ${boldon}${whitef} ▄█████▄▄ ${reset}       │  ${spc}${spc}${spc}   │
│ ${boldon}${whitef}███${cyanb}▀▀▀▀${blackb}▀${cyanb}▀${blackb}▀${cyanb}▀${reset}      │  Minggu, 15 April 2018 ${spc} │
│ ${boldon}${whitef}███${cyanb}▄   ${boldoff}${blackf}▀ ▀${reset}${cyanf}▀${reset}      │  Pukul 20:00 WIB       ${spc} │
│ ${boldon}${whitef} ▄${cyanb}  ${reset}${boldon}${whitef}█████▄ ${boldoff}${redf}█▄${reset}    │  ${spc}${spc}  ${spc} │
│ ${boldoff}${redf}▀▀${reset}${boldon}${redb}${whitef}▄${cyanb}▄   ${redb}▄▄▄${reset}${boldoff}${redf}▀██▀${reset}   │  Telegram : @dotfiles_id          │
│ ${boldon}${whitef} ██▀▀▀██▀  ${boldoff}${redf}▀${reset}     │  Pemateri : Pakde epsi ${spc} │
│ ${boldon}${whitef} ▀▀▀▀ ▀▀▀▀${reset}       │  Siapkan  : Baca log sebelumnya   │
└────${hrz}${hrz}${hrz}${hrz}${hrz}┘ 
EOF
