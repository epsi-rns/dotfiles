" -- -- ViM Standard -- -- 

set number
syntax on
set encoding=utf-8

" -- -- ViM UI -- --

set background=dark
set t_Co=256
if &term =~ '256color'
  " Disable Background Color Erase (BCE) so that color schemes
  " work properly when Vim is used inside tmux and GNU screen.
  " See also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif

set laststatus=2 " always show the status line
set lazyredraw " do not redraw while running macros
set showbreak=↪ " indicate wrapped line
hi clear ModeMsg " Disable Color StatusLine on Insert Mode and Visual Mode

" -- -- Vundle Begin -- --

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" -- -- Vundle Plugin -- --

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree'

" Plugin 'Lokaltog/powerline'


Plugin 'flazz/vim-colorschemes'
Plugin 'vim-airline/vim-airline'
" Plugin 'vim-airline/vim-airline-themes'


Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" -- -- Vundle End -- --

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" -- -- GUI Settings -- --

autocmd GUIEnter * set guioptions-=m
autocmd GUIEnter * set guioptions-=T
"autocmd GUIEnter * set gfn=Bitstream\ Vera\ Sans\ Mono\ 10
autocmd GUIEnter * set gfn=Monaco\ for\ Powerline\ 10
let g:Powerline_symbols = 'fancy'
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
autocmd GUIEnter * set vb t_vb= " disable visual bell
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
" let g:airline_theme='powerlineish'

" -- -- colorscheme -- --

hi Normal guibg=NONE ctermbg=NONE

colorscheme github
" colorscheme ir_black
" colorscheme evening
" colorscheme molokai

" -- -- NERDTree -- --

let NERDTreeShowHidden=1

