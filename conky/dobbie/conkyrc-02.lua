-- Author url = http://dobbie03.deviantart.com/art/My-First-Conky-Config-327206399
-- Modified by Akza
-- 11 June 2019: Modified by epsi

-- vim: ts=4 sw=4 noet ai cindent syntax=lua

--[[
Conky, a system monitor, based on torsmo
]]

home = os.getenv("HOME")
dofile(home .. '/conky/config.lua')
conky.config = configuration

datetime = ''
.. '${font CabinSketch-Bold:pixelsize=15}'
.. '${alignc}'
.. '${time [ %A, %I:%M:%S ]}'
.. '\n'
.. '${font CabinSketch-Bold:pixelsize=20}'
.. '${alignc}'
.. '${time %d %B, %Y}'
.. '\n'

message = ''
.. "${font CabinSketch-Bold:pixelsize=45}"
.. "${alignc}"
.. "Let's Get Married!"
.. "\n"

conky.text = ''
.. datetime
.. message 
