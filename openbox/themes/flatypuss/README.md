### SVG based theme for OpenBox

> For learning purpose.

Created April 2018 by E. Rizqi N. Sayidina <epsi.nurwijayadi@gmail.com>

-- -- --

### Why not Platypus ?

> With my respect for platypus, we still love you!

-- -- --

### Influence

This work had heavy influence from previous work

* Structure: based on example from [openbox.org/wiki/Help:Themes](http://openbox.org/wiki/Help:Themes)

* Boxed Icon Trick: based on [Addy's Block Theme](https://github.com/addy-dclxvi/openbox-theme-collections/tree/master/Blocks/openbox-3)

* Menu Border Trick: based on [Yuga's Arc Theme](https://github.com/noirecat/arc-ob-theme)

-- -- --

### Public Domain Dedication

Theme released under CC0 1.0 Universal (CC0 1.0) license.
* https://creativecommons.org/publicdomain/zero/1.0/legalcode
