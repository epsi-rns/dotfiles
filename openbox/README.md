Openbox
=====================

# Screenshots

## Configuration on Tutorial

[Tutorial][link-tutor]

<strong>OS</strong>: openSUSE<br/>
<strong>WM</strong>: Openbox<br/>
  + Tint2 (top: transparent: original theme)<br/>
  + Tint2 (bottom: solid: original theme)<br/>
  + OB3 Theme (Flatypuss: original theme)<br/>  
  + Menu (bunsenlabs, and obmenu generator)<br/>  
  + Icon: Numix Circle<br/>
  + compton transparency for conky and menu<br/>
  + Wallpaper: #! crunchbang<br/>
<br/>
::.. Tutorial ..::

![Openbox Screenshot Tutorial][photos-ss-openbox-tutor]

## Configuration on Arch Linux

<strong>OS</strong>: Arch<br/>
<strong>WM</strong>: Openbox<br/>
  + Kali Menu in openbox<br/>
  + Tint2 (custom color)<br/>
  + dockbarx<br/>
  + conky bar (manual modification)<br/>
  + Font: Capture It, Terminus<br/>
  + OB3 Theme: [Shiki Wine + Onyx, Modified]<br/>
  + Wallpaper: my own photo shot from my garden<br/>
  + tmux in xfce-terminal (no scrollbar)<br/>
  + cmus<br/>
  + screenfetch<br/>
  + compton transparency for conky and menu<br/>
<br/>
::.. Green Garden ..::

This screenshot use a lot of transparency.<br/>

![Openbox Screenshot Arch][picasa-ss-openbox-green]

<br/><br/>

## Configuration on Debian Linux

**OS**: Debian<br/>
**WM**: Openbox<br/>
<br/>

![Openbox Screenshot Debian (#! Clone)][picasa-ss-openbox-clone]

[link-tutor]: http://epsi-rns.github.io/desktop/2018/05/15/tint2-config.html

[picasa-ss-openbox-green]: https://lh3.googleusercontent.com/-IAK0W6hIhVg/Vz2oEHxs41I/AAAAAAAAARc/PioYvuCxLy8Tqy2h2LacXQoP-I4TdC_MgCCo/s0/openbox-green-garden.png

[picasa-ss-openbox-clone]: https://lh3.googleusercontent.com/-FfVK47gbWnI/Vz2n78YRaXI/AAAAAAAAARM/i2ix7ajBjhYrAk0VXi_EsPxa382GhzZ_ACCo/s0/openbox-arch-crunchbang-clone.png

[photos-ss-openbox-tutor]: https://lh3.googleusercontent.com/1Y_MT7lsPX_m0OzFYusNXzE6jcPksWfGt_HRFB0z9G6-3xuWPJFiyisMKSYz0DOSiXhf5WBQP1p-fk90oLP-DXEBKgJXiHctYaZXvhNpQAUTbuyZBbmS4qA6uxNQf3ABgz10GZ-gU82k4gUv-r-Lw7URKKS142jzOgBKl8OmF10sX7vsI7nUJVUtqzCMrOnAeW2U4fgTBH_SXs-xVIB2uyVLgcKkETof77wTnhfX7UCrLWWa27aLfIJGeA9XWEPYOWT7P7JAhLqi_A7EkFq-hI16A8f4igwWfq28M9UaeAY3LKtIMLT7GCodEByU-ehnoqzSUaIfSWdOCG_PRxjJU15VepcwxQrnc1gnbOE5kZqWo8Q_QjUg9nh4VPT0m3-WODzL7dTCNmyDhmo20qwUUqxcbvUTEmpxWSZGp8Ixpi_0LRf4RnU2qk3oKqwmd_3-P-qyw-c7Mge-axDosdL-xbFdJxHYzGAOeH8jneHJA9XXCPE6Z9OwwnBLzCL5WktSzQcGL64bfIdsBEKNlwJvnf3z6gDPXmirBejRTGkn-xmmwx5iTlzWG1p7lOV7R5_1FA9b2PPeg8PFC60IcXYBVza2e0uZTE5CiaoPMFxKtLgNyNKvd_dx7yDZxmq2m8_QOiAMCpqVelzNJ62JaCntPGkMwFcwt9S3=w800-h600-no
